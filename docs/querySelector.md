# Les querySelectors et le DOM

Dans un querySelector (sélecteur CSS également utilisé dans la fonction du même nom en JS (`document.querySelector(...)`, on peut utiliser :

- les classes (`.ma-classe`, etc.)
- les ids (`#mon-id`, etc.)
- les noms de balises  (div, etc.)
- les pseudo-classes (:hover, :active, etc.)
- les pseudo-éléments  (::after, ::selection, etc.)
- tous les opérateurs (combinateurs qui vont avec et qu'on ne va pas citer ici)

Tous ces "outils" peuvent être utilisés pour identifier / faire référence à des objets du DOM (Document Object Model) qui est une représentation de notre page HTML, optimisée par notre navigateur pour qu'il s'y retrouve et puisse manipuler notre page efficacement.
